

视频链接：https://www.bilibili.com/video/BV1mE411u7zg

我下载的是cuda10和对应版本的cudnn

[![r35XyF.png](https://s3.ax1x.com/2020/12/17/r35XyF.png)](https://imgchr.com/i/r35XyF)

然后把cudnn中的文件解压到cuda对应的文件中。



[![r3IFSK.png](https://s3.ax1x.com/2020/12/17/r3IFSK.png)](https://imgchr.com/i/r3IFSK)



安装tensorflow-gpu:1.13.2

如果不行：换源多尝试几次，最终应该是能成功的

```python
pip --default-timeout=60 install tensorflow-gpu==1.14 -i https://pypi.mirrors.ustc.edu.cn/simple/
```

安装keras:2.1.5，这个应该好下载

```python
pip --default-timeout=60 install keras==2.1.5 -i https://pypi.mirrors.ustc.edu.cn/simple/
```



# 遇到的问题

[![r35PKg.png](https://s3.ax1x.com/2020/12/17/r35PKg.png)](https://imgchr.com/i/r35PKg)

解决办法：https://blog.csdn.net/daihaoxin/article/details/109788726

```python
pip install numpy == 1.17.4
```



问题：InternalError: Blas GEMM launch failed : a.shape=(100, 784), b.shape=(784, 10), m=100, n=10...解决方法

我拿keras mnist手写数字识别的例子来运行测试效果，结果报上面的错误，说是显存不够，开了其他脚本。我电脑刚开机...

解决方法：https://blog.csdn.net/qq_43061705/article/details/102649005，这里下载4个补丁我没有尝试过。

换了一个逻辑回归的例子，能够正常跑。

mnist的例子在colab上可以正常运行，不知道如何解决。

https://blog.csdn.net/ykf173/article/details/105542936



https://bbs.csdn.net/topics/396518756

Loaded runtime CuDNN library: 7.4.1 but source was compiled with: 7.6.0.

升级cudnn



应该是tf对应cuda的版本

tf 2.4.0

keras 2.4.3

numpy 1.19.3

cuda 11.0

cudnn 8.0.5  https://developer.nvidia.com/rdp/cudnn-download



如果提示could not create cudnn handle: CUDNN_STATUS_NOT_INITIALIZED

可能没有以下这句

```
# 手写数字识别
import tensorflow as tf
config = tf.compat.v1.ConfigProto(allow_soft_placement = True)
config.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config = config)
```

而不是网上说的什么cuda和cudnn版本不匹配问题。

