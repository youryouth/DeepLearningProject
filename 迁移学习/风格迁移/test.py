import numpy as np
from keras.applications import vgg16
from keras.preprocessing.image import load_img, img_to_array
from keras import backend as K
import tensorflow as tf
from scipy.optimize import fmin_l_bfgs_b
import time

from imageio import imwrite,imsave
config = tf.compat.v1.ConfigProto(allow_soft_placement = True)
config.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config = config)
tf.compat.v1.disable_eager_execution()
x = np.arange(0,10,0.1)
m_true = 2.5
b_true = 1.0
y_true = m_true*x + b_true

def func(params, *args):
    x = args[0]
    y = args[1]
    m, b = params
    y_model = m*x+b
    # y=y_true,优化得到一个m和b
    error = y-y_model
    return sum(error**2)

initial_values = np.array([1.0, 0.0])
mybounds = [(None,2), (None,None)]

# 第一个优化是无限的，并且给出了正确的答案，第二个优化考虑了阻止它达到正确参数的界限
fmin_l_bfgs_b(func, x0=initial_values, args=(x,y_true), approx_grad=True)

fmin_l_bfgs_b(func, x0=initial_values, args=(x, y_true), bounds=mybounds, approx_grad=True)